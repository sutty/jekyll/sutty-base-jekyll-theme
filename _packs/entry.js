import { Notifier } from "@airbrake/browser";

if (window.env.JEKYLL_ENV === "production") {
  try {
    window.airbrake = new Notifier({
      projectId: window.env.AIRBRAKE_PROJECT_ID,
      projectKey: window.env.AIRBRAKE_PROJECT_KEY,
      host: "https://panel.sutty.nl",
    });

    console.originalError = console.error;
    console.error = (...e) => {
      window.airbrake.notify(e.join(" "));
      return console.originalError(...e);
    };
  } catch (e) {
    console.error(e);
  }
}

import * as Turbo from "@hotwired/turbo";
Turbo.start();

import { Application } from "@hotwired/stimulus";
window.Stimulus = Application.start();
window.abortController = undefined;

import BodyScrollController from "./controllers/body_scroll_controller";
import DeviceDetectorController from "./controllers/device_detector_controller";
import ModalController from "./controllers/modal_controller";
import AuthorizeInteractionController from "./controllers/authorize_interaction_controller";
import TabsController from "./controllers/tabs_controller";

Stimulus.debug = window.env.JEKYLL_ENV !== "production";
Stimulus.register("body-scroll", BodyScrollController);
Stimulus.register("device-detector", DeviceDetectorController);
Stimulus.register("modal", ModalController);
Stimulus.register("authorize-interaction", AuthorizeInteractionController);
Stimulus.register("tabs", TabsController);

document.addEventListener("turbo:load", (event) => {
  document
    .querySelectorAll("a[href^='http://'],a[href^='https://'],a[href^='//']")
    .forEach((a) => {
      a.rel = "noopener";
      a.target = "_blank";
    });

  window.abortController = new AbortController();
});

document.addEventListener("turbo:visit", (event) => {
  window.abortController.abort();
});
